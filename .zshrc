HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory
bindkey -e
zstyle :compinstall filename '/home/sunny/.zshrc'

autoload -Uz compinit
compinit

bindkey '^[[H' beginning-of-line
bindkey '^[[F' end-of-line

PS1="%n@%M:%d %% "
