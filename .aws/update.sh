#!/bin/sh

# Poor man's AWS creds "management"

if [ -e credentials ] ; then
    if [ -L credentials ] ; then
        rm credentials
    else
        echo "credentials not a link, exiting"
        exit 1
    fi
fi

ln -s "${1}" credentials
