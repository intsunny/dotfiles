" The majority of these settings came from a particular internet page
" I found on Google. Unfortunately I cannot remember what page and
" thus I cannot credit properly :(
" Sunny

set nocompatible                " vi-compatible mode is not good
set viminfo+=!                  " make sure it can save viminfo
set backup                      " make backup file
set backupdir=~/.vim/backups    " where to put backup files
set backupdir=~/.vim/temps      " where to put temp files
set number                      " This turns on line numbering
nmap <C-A><C-A> :q!<CR>         " type C-W C-W to quit any file
set ttymouse=xterm2             " Set mouse type to work in screen
set mouse=a                     " This enables visual mouse support
set clipboard+=unnamed          " Allows vim to use the OS clipboard

set history=700                 " Have vim track the last 700 commands
filetype plugin on              " Enabile filetype plugins
filetype indent on              " Indent depending on the file type
set autoread                    " Autoread when file is externally modified
set wildignore=*.o,*~,*.pyc     " Ignore compiled files
set ruler                       " Always show current position
set backspace=eol,start,indent  " Configure backspace so it acts much better
set whichwrap+=<,>,h,l          " Allow for word wrapping
set ignorecase                  " Ignore case when searching
set smartcase                   " When searching try to be smart about cases 
set hlsearch                    " Highlight search results
set incsearch                   " Makes search act like search in modern browsers
set lazyredraw                  " Don't redraw while executing macros (good performance config)
set magic                       " For regular expressions turn magic on
set showmatch                   " Show matching brackets when text indicator is over them
set mat=2                       " How many tenths of a second to blink when matching brackets

syntax enable                   " Enable syntax highlighting
if has("gui_running")           " Set extra options when running in GUI mode
    set guioptions-=T
    set guioptions+=e
    set t_Co=256
    set guitablabel=%M\ %t
endif

set expandtab                   " Use spaces instead of tabs
set smarttab                    " Be smart when using tabs ;)
set shiftwidth=2                " 1 tab == 4 spaces
set tabstop=2

set autoindent                  "Auto indent
set smartindent                 "Smart indent
set wrap                        "Wrap lines

autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p
